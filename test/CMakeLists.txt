PROJECT (PhysicsSimTest)

FIND_PACKAGE(Boost 1.35 COMPONENTS unit_test_framework REQUIRED)

ADD_DEFINITIONS(-DBOOST_TEST_DYN_LINK=1)

ADD_EXECUTABLE(proximity_list_test proximity_list_test.cpp)
TARGET_LINK_LIBRARIES(proximity_list_test ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})
ADD_TEST(proximity_list ${CMAKE_CURRENT_BINARY_DIR}/proximity_list_test)
