#include "../src/util/proximity_list.hpp"
#define BOOST_TEST_MODULE ProximityListTest
#include <boost/test/unit_test.hpp>
#include <boost/bind.hpp>
#include <boost/random.hpp>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

struct Distance1
{
    typedef float result_type;

    template<class T>
    result_type operator()(const T &left, const T &right) const
    {
        return std::abs(left - right);
    }
};

BOOST_AUTO_TEST_CASE(constructors)
{
    proximity_list<int, Distance1> p1;

    BOOST_CHECK(p1.begin() == p1.end());

    std::vector<int> vec;
    for(size_t i = 0; i < 10; ++i)
    {
        vec.push_back(i);
    }

    proximity_list<int, Distance1> p2(vec.begin(), vec.end());

    BOOST_CHECK_EQUAL(vec.size(), p2.size());

}
BOOST_AUTO_TEST_CASE(insert)
{
    proximity_list<int, Distance1> p1;
    p1.insert(1);
    p1.insert(2);
    p1.insert(3);

    BOOST_CHECK_EQUAL(p1.size(), 3);

    std::vector<int> vec;
    for(size_t i = 0; i < 10; ++i)
    {
        vec.push_back(i);
    }
    p1.insert(vec.begin(), vec.end());

    BOOST_CHECK_EQUAL(p1.size(), 13);
}
BOOST_AUTO_TEST_CASE(find_nearest)
{
    boost::mt19937 rng;
    boost::uniform_int<> size(0, 100);
    boost::variate_generator<boost::mt19937 &, boost::uniform_int<> > random(
        rng, size);

    std::vector<int> vec;
    for(size_t i = 0; i < 10; ++i)
    {
        vec.push_back(random());
    }
    proximity_list<int, Distance1> p1(vec.begin(), vec.end());

    for(size_t i = 0; i < 100; ++i)
    {
        int query = random();
        int nearest1 = *p1.find_nearest(query);
        int nearest2 = *std::max_element(vec.begin(), vec.end(), boost::bind(
            std::less<Distance1::result_type>(),
            boost::bind(Distance1(), _2, query),
            boost::bind(Distance1(), _1, query)));

        BOOST_CHECK_EQUAL(nearest1, nearest2);
    }
}
