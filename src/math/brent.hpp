/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2008 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BRENT_HPP
#define BRENT_HPP

#include "constants.hpp"
#include <boost/function.hpp>
#include <limits>

template<class T>
class Brent
{
public:
    void operator()(const T & ax, const T & bx, const T & cx,
        boost::function<T (const T &)> f, T & xmin) const;
private:
    static const T TOLERANCE;
};

template<class T>
const T Brent<T>::TOLERANCE = std::sqrt(std::numeric_limits<T>::epsilon());

template<class T>
void Brent<T>::operator()(const T & ax, const T & bx, const T & cx,
    boost::function<T (const T &)> f, T & xmin) const
{
    T left;
    T right;
    if(ax < cx)
    {
        left = ax;
        right = cx;
    }
    else
    {
        left = cx;
        right = ax;
    }

    T thirdBest;
    T secondBest;
    T best;
    T mostRecent;

    thirdBest = secondBest = best = bx;

    T fThirdBest;
    T fSecondBest;
    T fBest;

    fThirdBest = fSecondBest = fBest = f(best);

    T e = T();
    T ePrevious = T();
    T d = T();

    T middle = left + (right - left) * T(0.5);
    T tolerance = TOLERANCE * std::abs(best) +
        std::numeric_limits<T>::epsilon();

    while(std::abs(best - middle) > tolerance * T(2) - T(0.5) * (right - left))
    {
        //std::cout << "BRENT\n";
        T numerator = T();
        T denominator = T();

        if(std::abs(e) > tolerance)
        {
            numerator = std::pow(best - thirdBest, 2) * (fBest - fSecondBest) -
                std::pow(best - secondBest, 2) * (fBest - fThirdBest);
            denominator = T(2) * ((best - thirdBest) * (fBest - fSecondBest) -
                (best - secondBest) * (fBest - fThirdBest));

            if(denominator > T())
            {
                numerator = -numerator;
            }
            else
            {
                denominator = -denominator;
            }
            ePrevious = e;
            e = d;
        }

        if(std::abs(numerator) < std::abs(T(0.5) * denominator * ePrevious) ||
            numerator > denominator * (left - best) ||
            numerator < denominator * (right - best))
        {
            d = numerator / denominator;
            mostRecent = best + d;

            if(mostRecent - left < tolerance * T(2) ||
                right - mostRecent < tolerance * T(2))
            {
                d = best < middle ? tolerance : -tolerance;
            }
        }
        else
        {
            e = best < middle ? right - best : left - best;
            d = (T(1) - Constants<T>::INV_PHI) * e;
        }

        if(std::abs(d) >= tolerance)
        {
            mostRecent = best + d;
        }
        else if(d > T())
        {
            mostRecent = best + tolerance;
        }
        else
        {
            mostRecent = best - tolerance;
        }

        T fMostRecent = f(mostRecent);
        if(fMostRecent <= fBest)
        {
            if(mostRecent < best)
            {
                right = best;
            }
            else
            {
                left = best;
            }
            thirdBest = secondBest;
            fThirdBest = fSecondBest;
            secondBest = best;
            fSecondBest = fBest;
            best = mostRecent;
            fBest = fMostRecent;
        }
        else
        {
            if(mostRecent < best)
            {
                left = mostRecent;
            }
            else
            {
                right = mostRecent;
            }

            if(fMostRecent <= fSecondBest || secondBest == best)
            {
                thirdBest = secondBest;
                fThirdBest = fSecondBest;
                secondBest = mostRecent;
                fSecondBest = fMostRecent;
            }
            else if(fMostRecent <= fThirdBest || thirdBest == best ||
                thirdBest == secondBest)
            {
                thirdBest = mostRecent;
                fThirdBest = fMostRecent;
            }
        }
        middle = left + (right - left) * T(0.5);
        tolerance = TOLERANCE * std::abs(best) +
            std::numeric_limits<T>::epsilon();
    }

    xmin = best;
}

#endif // BRENT_HPP
