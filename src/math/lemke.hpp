/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef LEMKE_HPP
#define LEMKE_HPP

#include "../util/increment.hpp"
#include "../boost/invert_matrix.hpp"
#include <boost/iterator/filter_iterator.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/foreach.hpp>
#include <algorithm>
#include <functional>
#include <iterator>

/*!
 * \class Lemke lemke.hpp "math/lemke.hpp"
 * \brief This functor implements the Lemke Complementary Pivot Algorithm for
 * solving Linear Complementarity Problems (LCPs).
 *
 * Implementation of the algorithm as described in K. G. Murty, "Linear
 * Complementarity, Linear and Nonlinear Programming", Internet Edition, 1997,
 * pp. 62-84.\n
 * http://ioe.engin.umich.edu/people/fac/books/murty/linear_complementarity_webbook/
 *
 * \tparam T the scalar type.
 */
template<class T>
class Lemke
{
public:
    /*!
     * \brief Input: matrix m and vector q. Output: vectors w and z.
     *
     * Given an input of a matrix \f$ \mathbf{M} \f$ and vector \f$ \mathbf{q}
     * \f$, the algorithm attempts to find two vectors \f$ \mathbf{w} \f$ and
     * \f$ \mathbf{z} \f$ which satisfy the following conditions:\n\n
     * \f$ \mathbf{w} - \mathbf{Mz} = \mathbf{q} \f$\n
     * \f$ \mathbf{w} \ge 0, \mathbf{z} \ge 0 \f$\n
     * \f$ \mathbf{w}^{\mathrm{T}}\mathbf{z} = 0 \f$
     *
     * \tparam A storage array.
     * \param q vector \f$ \mathbf{q} \f$.
     * \param m matrix \f$ \mathbf{M} \f$.
     * \param w vector \f$ \mathbf{w} \f$.
     * \param z vector \f$ \mathbf{z} \f$.
     */
    template<class A>
    bool operator()(const boost::numeric::ublas::vector<T, A> & q,
        const boost::numeric::ublas::matrix<T> & m,
        boost::numeric::ublas::vector<T, A> & w,
        boost::numeric::ublas::vector<T, A> & z);

private:
    struct is_non_negative
    {
        bool operator()(const T &value)
        {
            return T() < value;
        }
    };
};

template<class T> template<class A>
bool Lemke<T>::operator()(const boost::numeric::ublas::vector<T, A> & q,
    const boost::numeric::ublas::matrix<T> & m,
    boost::numeric::ublas::vector<T, A> & w,
    boost::numeric::ublas::vector<T, A> & z)
{
    typedef typename boost::numeric::ublas::vector<T>::size_type size_type;
    size_type dimension = q.size();

    // Trivial case.
    if(std::find_if(q.begin(), q.end(), std::bind2nd(std::less<T>(), T())) ==
        q.end())
    {
        w.assign(q);
        z.assign(boost::numeric::ublas::zero_vector<T>(dimension));

        return true;
    }

    boost::numeric::ublas::matrix<T> tableau(dimension, (dimension << 1) + 2);

    boost::numeric::ublas::matrix_range<boost::numeric::ublas::matrix<T> > p0 =
        subrange(tableau, 0, dimension, 0, dimension);
    boost::numeric::ublas::matrix_range<
        boost::numeric::ublas::matrix<T> > negativeMDash = subrange(tableau, 0,
        dimension, dimension, dimension << 1);
    boost::numeric::ublas::matrix_column<
        boost::numeric::ublas::matrix<T> > z0 = column(tableau,
        dimension << 1);
    boost::numeric::ublas::matrix_column<
        boost::numeric::ublas::matrix<T> > qDash = column(tableau,
        (dimension << 1) + 1);

    p0.assign(boost::numeric::ublas::identity_matrix<T>(dimension));
    z0.assign(boost::numeric::ublas::scalar_vector<T>(dimension, T(-1)));
    size_type minQ = std::distance(q.begin(), std::min_element(q.begin(),
        q.end()));
    z0.swap(column(tableau, minQ));

    negativeMDash.assign(-prod(p0, m));
    qDash.assign(prod(p0, q));

    std::vector<size_type> basic(dimension);
    std::generate(basic.begin(), basic.end(), Increment<size_type>());
    basic[minQ] = dimension << 1;

    // TODO: Create a function similar to subrange that will select arbitrary
    // indices of a matrix specified in a range [first, last).
    boost::numeric::ublas::matrix<T> basis(dimension, dimension);
    for(size_type i = 0; i < dimension; ++i)
    {
        column(basis, i) = column(tableau, basic[i]);
    }

    boost::numeric::ublas::matrix<T> stage(dimension, dimension + 2);
    boost::numeric::ublas::matrix_column<
        boost::numeric::ublas::matrix<T> > pivotColumn = column(stage, 0);
    boost::numeric::ublas::matrix_column<
        boost::numeric::ublas::matrix<T> > qBar = column(stage, 1);
    boost::numeric::ublas::matrix_range<
        boost::numeric::ublas::matrix<T> > basisInverse = subrange(stage, 0,
        dimension, 2, dimension + 2);
    basisInverse.assign(basis);

    size_type pivotColumnIndex = minQ + dimension;
    pivotColumn.assign(column(tableau, pivotColumnIndex));

    qBar.assign(qDash);

    typedef boost::filter_iterator<is_non_negative,
        typename boost::numeric::ublas::matrix<T>::iterator1> FilterIterator;
    while(std::find(basic.begin(), basic.end(), dimension << 1) != basic.end())
    {
        FilterIterator first(stage.begin1(), stage.end1());
        FilterIterator last(stage.end1(), stage.end1());

        if(first == last)
        {
            std::cout << "RAY TERMINATION!\n";
            std::cout << "q: " << q << std::endl;
            std::cout << "M: " << m << std::endl;
            return false;
        }

        FilterIterator lexicoMinimum = first;
        while(++first != last)
        {
            if(std::lexicographical_compare(first.base().begin()+1,
                first.base().end(), lexicoMinimum.base().begin()+1,
                lexicoMinimum.base().end()))
            {
                lexicoMinimum = first;
            }
        }

        size_type pivotRowIndex = std::distance(stage.begin1(),
            lexicoMinimum.base());

        column(basis, pivotRowIndex) = column(tableau, pivotColumnIndex);
        std::swap(pivotColumnIndex, basic[pivotRowIndex]);
        pivotColumnIndex = pivotColumnIndex < dimension ?
            pivotColumnIndex + dimension : pivotColumnIndex - dimension;

        if(!invert(basis, basisInverse))
        {
            std::cout << "INVERSION FAILED!\n";
            std::cout << "basis: " << basis << std::endl;
            return false;
        }

        pivotColumn = prod(basisInverse, column(tableau, pivotColumnIndex));

        qBar = prod(basisInverse, qDash);
    }

    for(typename std::vector<size_type>::const_iterator i = basic.begin();
        i != basic.end(); ++i)
    {
        if(*i < dimension)
        {
            w(*i) = qBar(*i);
            z(*i) = T();
        }
        else
        {
            w(*i - dimension) = T();
            z(*i - dimension) = qBar(*i - dimension);
        }
    }
    return true;
}

#endif // LEMKE_HPP
