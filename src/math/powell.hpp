/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2008 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef POWELL_HPP
#define POWELL_HPP

#include "bracket.hpp"
#include "brent.hpp"
#include "constants.hpp"
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>

template<class T, class OneDimensionalOptimiser = Brent<T> >
class Powell
{
public:
    void operator()(boost::numeric::ublas::vector<T> & point,
        boost::function<T (const boost::numeric::ublas::vector<T> &)> f);
private:
    static T oneDimensional(const boost::numeric::ublas::vector<T> point,
        const T & lambda, const boost::numeric::ublas::vector<T> & direction,
        boost::function<T (const boost::numeric::ublas::vector<T> &)> f)
    {
        return f(point + lambda * direction);
    }
    static const OneDimensionalOptimiser optimiser_;
    static const T TOLERANCE;
};

template<class T, class OneDimensionalOptimiser>
const OneDimensionalOptimiser Powell<T, OneDimensionalOptimiser>::optimiser_ =
    OneDimensionalOptimiser();

template<class T, class OneDimensionalOptimiser>
const T Powell<T, OneDimensionalOptimiser>::TOLERANCE = std::sqrt(
    std::numeric_limits<T>::epsilon());

template<class T, class OneDimensionalOptimiser>
void Powell<T, OneDimensionalOptimiser>::operator()(
    boost::numeric::ublas::vector<T> & point,
    boost::function<T (const boost::numeric::ublas::vector<T> &)> f)
{
    boost::numeric::ublas::vector<T> best(point.size());
    boost::numeric::ublas::vector<T> pt(point);

    T fret = f(point);

    T fp;
    T fptt;
    size_t index;
    T decrease;

    boost::numeric::ublas::matrix<T> directions(
        boost::numeric::ublas::identity_matrix<T>(point.size(), point.size()));
    
    T bracketA;
    T bracketB;
    T bracketC;

    while(T(2) * (fp - fret) > TOLERANCE * (std::abs(fp) + std::abs(fret)) + 1e-20 /*TINY*/)
    {
        fp = fret;
        index = 0;
        decrease = T();
        
        for(size_t i = 0; i < directions.size1(); ++i)
        {
            bracketA = T();
            bracketB = T(1);
            bracket<T>(bracketA, bracketB, bracketC,
                boost::bind(oneDimensional, point, _1, column(directions, i), f));
            T xmin;
            fptt = fret;
            optimiser_(bracketA, bracketB, bracketC,
                boost::bind(oneDimensional, point, _1, column(directions, i), f), xmin);
            point += column(directions, i) * xmin;
            fret = f(point);
            if(fptt - fret > decrease)
            {
                decrease = fptt - fret;
                index = i;
            }
        }

        boost::numeric::ublas::vector<T> xit(point - pt);
        fptt = f(T(2) * point - pt);
        pt = point;

        if(fptt < fp)
        {
            T t = T(2) * (fp - T(2) * fret + fptt) *
                std::pow(fp - fret - decrease, 2) - decrease *
                std::pow(fp - fptt, 2);
            if(t < T())
            {
                bracketA = T();
                bracketB = T(1);
                bracket<T>(bracketA, bracketB, bracketC, boost::bind(oneDimensional,
                    point, _1, xit, f));
                T xmin;
                optimiser_(bracketA, bracketB, bracketC, boost::bind(oneDimensional,
                    point, _1, xit, f), xmin);
                point += xit * xmin;
                fret = f(point);
                column(directions, index) = column(directions, point.size() - 1);
                column(directions, point.size() - 1) = xit;
            }
        }
    }
}


#endif //POWELL_HPP
