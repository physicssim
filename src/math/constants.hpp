/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <cmath>
#include <limits>

template<class T>
class Constants
{
public:
    static const T &pi();
    static const T &phi();
    static const T &ln2();
    static const T RAD_TO_DEG;
    static const T DEG_TO_RAD;
    static const T INV_PHI;
    static const T INV_LN_2;
    static const T RELATIVE_ERROR_TOLERANCE;
};

template<class T>
inline const T &Constants<T>::pi()
{
    static const T pi = std::acos(T(-1));

    return pi;
}
template<class T>
inline const T &Constants<T>::phi()
{
    static const T phi = (T(1) + std::sqrt(T(5))) / T(2);

    return phi;
}
template<class T>
inline const T &Constants<T>::ln2()
{
    static const T ln2 = std::log(T(2));

    return ln2;
}
template<class T>
const T Constants<T>::RAD_TO_DEG = T(180) / pi();
template<class T>
const T Constants<T>::DEG_TO_RAD = pi() / T(180);
template<class T>
const T Constants<T>::INV_PHI = T(1) / phi();
template<class T>
const T Constants<T>::INV_LN_2 = T(1) / ln2();
template<class T>
const T Constants<T>::RELATIVE_ERROR_TOLERANCE = std::sqrt(
    std::numeric_limits<T>::epsilon());

#endif // CONSTANTS_HPP
