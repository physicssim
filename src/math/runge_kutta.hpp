/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2007 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef RUNGE_KUTTA_HPP
#define RUNGE_KUTTA_HPP

#include <boost/function.hpp>

/*!
 * \class RungeKutta runge_kutta.hpp "math/runge_kutta.hpp"
 * \brief This functor implements the Runge-Kutta method for solving Ordinary
 *        Differential Equations (ODEs).
 *
 * \tparam T function input type.
 * \tparam Y function output type.
 */
template<class T, class Y>
class RungeKutta
{
public:
    /*!
     * \param t evaluation point.
     * \param dt step size.
     * \param y evaluated value.
     * \param f evaluation function.
     */
    void operator()(const T &t, const T &dt, Y &y,
        boost::function<void (const T &, Y &)> f)
    {
        Y k1 = y;
        f(t, k1);

        Y k2 = y + k1 * dt * T(0.5);
        f(t + dt * T(0.5), k2);

        Y k3 = y + k2 * dt * T(0.5);
        f(t + dt * T(0.5), k3);

        Y k4 = y + k3 * dt;
        f(t + dt, k4);

        y += dt * RK_COEFFICIENT * (k1 + T(2.0) * (k2 + k3) + k4);
    }
private:
    static const T RK_COEFFICIENT;
};

template<class T, class Y>
const T RungeKutta<T, Y>::RK_COEFFICIENT = T(1.0) / T(6.0);

#endif // RUNGE_KUTTA_HPP
