/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2008 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BRACKET_HPP
#define BRACKET_HPP

#include <boost/function.hpp>

template<class T>
void bracket(T & ax, T & bx, T & cx, boost::function<T (const T &)> f)
{
    T ulim, u, r, q, fu;

    T fa = f(ax);
    T fb = f(bx);

    if(fa < fb)
    {
        std::swap(ax, bx);
        std::swap(fa, fb);
    }
    cx = bx + Constants<T>::phi() * (bx - ax);
    T fc = f(cx);

    while(fc < fb)
    {
        r = (bx - ax) * (fb - fc);
        q = (bx - cx) * (fb - fa);
        u = bx - ((bx - cx) * q - (bx - ax) * r) / (T(2) * sign(std::max(q - r,
            1e-20f /*TINY*/), q - r));
        ulim = bx + 100 /*GLIMIT*/ * (cx - bx);

        if((bx - u) * (u - cx) > T())
        {
            fu = f(u);

            if(fu < fc)
            {
                ax = bx;
                bx = u;
                fa = fb;
                fb = fu;
                return;
            }
            else if(fb < fu)
            {
                cx = u;
                fc = fu;
                return;
            }
            u = cx + Constants<T>::phi() * (cx - bx);
            fu = f(u);
        }
        else if((cx - u) * (u - ulim) > T())
        {
            fu = f(u);

            if(fu < fc)
            {
                bx = cx;
                cx = u;
                u = cx + Constants<T>::phi() * (cx - bx);
                fb = fc;
                fc = fu;
                fu = f(u);
            }
        }
        else if((u - ulim) * (ulim - cx) >= T())
        {
            u = ulim;
            fu = f(u);
        }
        else
        {
            u = cx + Constants<T>::phi() * (cx - bx);
            fu = f(u);
        }
        ax = bx;
        bx = cx;
        cx = u;

        fa = fb;
        fb = fc;
        fc = fu;
    }
}

#endif // BRACKET_HPP
