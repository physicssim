/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Elsevier CDROM license agreement grants nonexclusive license to use the
 * software for any purpose, commercial or non-commercial as long as the
 * following credit is included identifying the original source of the
 * software:
 *
 * Parts of the source are "from the book Real-Time Collision Detection by
 * Christer Ericson, published by Morgan Kaufmann Publishers, (c) 2005
 * Elsevier Inc."
 */

#ifndef SIMPLEX_HPP
#define SIMPLEX_HPP

#include "../util/equal_vector.hpp"
#include "../util/less_length_squared.hpp"
#include <boost/numeric/ublas/vector.hpp>
#include <vector>
#include <algorithm>

template<class T>
class Simplex
{
public:
    size_t size() const;
    void clear();
    boost::numeric::ublas::bounded_vector<T, 3> closestPointA() const;
    boost::numeric::ublas::bounded_vector<T, 3> closestPointB() const;
    boost::numeric::ublas::bounded_vector<T, 3> closestPointC() const;
    T maxDotProduct() const;
    void addPoint(const boost::numeric::ublas::bounded_vector<T, 3> &pointA,
        const boost::numeric::ublas::bounded_vector<T, 3> &pointB);
    bool inSimplex(
        const boost::numeric::ublas::bounded_vector<T, 3> &point) const;
    void getClosest();
private:
    void barycentric(const boost::numeric::ublas::bounded_vector<T, 3> &a,
        const boost::numeric::ublas::bounded_vector<T, 3> &b,
        const boost::numeric::ublas::bounded_vector<T, 3> &c,
        const boost::numeric::ublas::bounded_vector<T, 3> &p);
    void closestPointLine();
    void closestPointTriangle();
    void closestPointTetrahedron();

    std::vector<boost::numeric::ublas::bounded_vector<T, 3> > simplexA_;
    std::vector<boost::numeric::ublas::bounded_vector<T, 3> > simplexB_;
    // Minkowski sum A + (-B).
    std::vector<boost::numeric::ublas::bounded_vector<T, 3> > simplexC_;
    // Geometric centroid of simplex.
    std::vector<T> barycentric_;
};

template<class T>
size_t Simplex<T>::size() const
{
    return simplexC_.size();
}
template<class T>
void Simplex<T>::clear()
{
    simplexA_.clear();
    simplexB_.clear();
    simplexC_.clear();
    barycentric_.clear();
}
template<class T>
boost::numeric::ublas::bounded_vector<T, 3> Simplex<T>::closestPointA() const
{
    boost::numeric::ublas::bounded_vector<T, 3> closest;
    closest.clear();
    if(this->size() >= barycentric_.size())
    {
        for(size_t i = 0; i < barycentric_.size(); ++i)
        {
            closest += barycentric_[i] * simplexA_[i];
        }
    }
    return closest;
}
template<class T>
boost::numeric::ublas::bounded_vector<T, 3> Simplex<T>::closestPointB() const
{
    boost::numeric::ublas::bounded_vector<T, 3> closest;
    closest.clear();
    if(this->size() >= barycentric_.size())
    {
        for(size_t i = 0; i < barycentric_.size(); ++i)
        {
            closest += barycentric_[i] * simplexB_[i];
        }
    }
    return closest;
}
template<class T>
boost::numeric::ublas::bounded_vector<T, 3> Simplex<T>::closestPointC() const
{
    boost::numeric::ublas::bounded_vector<T, 3> closest;
    closest.clear();
    if(this->size() >= barycentric_.size())
    {
        for(size_t i = 0; i < barycentric_.size(); ++i)
        {
            closest += barycentric_[i] * simplexC_[i];
        }
    }
    return closest;
}
template<class T>
T Simplex<T>::maxDotProduct() const
{
    boost::numeric::ublas::bounded_vector<T, 3> max;
    max = *max_element(simplexC_.begin(), simplexC_.end(),
        LessLengthSquared());

    return inner_prod(max, max);
}
template<class T>
void Simplex<T>::barycentric(
    const boost::numeric::ublas::bounded_vector<T, 3> &a,
    const boost::numeric::ublas::bounded_vector<T, 3> &b,
    const boost::numeric::ublas::bounded_vector<T, 3> &c,
    const boost::numeric::ublas::bounded_vector<T, 3> &p)
{
    boost::numeric::ublas::bounded_vector<T, 3> v0 = b - a;
    boost::numeric::ublas::bounded_vector<T, 3> v1 = c - a;
    boost::numeric::ublas::bounded_vector<T, 3> v2 = p - a;

    T d00 = inner_prod(v0, v0);
    T d01 = inner_prod(v0, v1);
    T d11 = inner_prod(v1, v1);
    T d20 = inner_prod(v2, v0);
    T d21 = inner_prod(v2, v1);

    T denominator = d00 * d11 - d01 * d01;
    T inverseDenominator = denominator != T() ? T(1) / denominator : T();
    T v = (d11 * d20 - d01 * d21) * inverseDenominator;
    T w = (d00 * d21 - d01 * d20) * inverseDenominator;
    T u = T(1) - v - w;

    barycentric_.clear();
    barycentric_.push_back(w);
    barycentric_.push_back(v);
    barycentric_.push_back(u);
}
template<class T>
void Simplex<T>::addPoint(
    const boost::numeric::ublas::bounded_vector<T, 3> &pointA,
    const boost::numeric::ublas::bounded_vector<T, 3> &pointB)
{
    simplexA_.push_back(pointA);
    simplexB_.push_back(pointB);
    simplexC_.push_back(pointA - pointB);
}
template<class T>
bool Simplex<T>::inSimplex(
    const boost::numeric::ublas::bounded_vector<T, 3> &point) const
{
    return std::find_if(simplexC_.begin(), simplexC_.end(),
        bind1st(EqualVector<boost::numeric::ublas::bounded_vector<T, 3> >(),
        point)) != simplexC_.end();
}
template<class T>
void Simplex<T>::getClosest()
{
    if(this->size() == 1)
    {
        barycentric_.clear();
        barycentric_.push_back(T(1));
    }
    if(this->size() == 2)
    {
        closestPointLine();
    }
    if(this->size() == 3)
    {
        closestPointTriangle();
    }
    if(this->size() == 4)
    {
        closestPointTetrahedron();
        // TODO: Need to set barycentric coordinates here.
    }
}
template<class T>
void Simplex<T>::closestPointLine()
{
    boost::numeric::ublas::bounded_vector<T, 3> a = simplexC_[1];
    boost::numeric::ublas::bounded_vector<T, 3> b = simplexC_[0];
    boost::numeric::ublas::bounded_vector<T, 3> ab = b - a;
    boost::numeric::ublas::bounded_vector<T, 3> ao = -a;

    if(inner_prod(ab, ao) > T())
    {
        // [A,B]
        T t = inner_prod(ab, ao) / inner_prod(ab, ab);
        barycentric_.clear();
        barycentric_.push_back(t);
        barycentric_.push_back(T(1.0) - t);
    }
    else
    {
        // [A]
        simplexA_.erase(simplexA_.begin());
        simplexB_.erase(simplexB_.begin());
        simplexC_.erase(simplexC_.begin());
        barycentric_.clear();
        barycentric_.push_back(T(1.0));
    }
}
template<class T>
void Simplex<T>::closestPointTriangle()
{
    boost::numeric::ublas::bounded_vector<T, 3> a = simplexC_[2];
    boost::numeric::ublas::bounded_vector<T, 3> b = simplexC_[1];
    boost::numeric::ublas::bounded_vector<T, 3> c = simplexC_[0];
    boost::numeric::ublas::bounded_vector<T, 3> ao = -a;
    boost::numeric::ublas::bounded_vector<T, 3> ab = b - a;
    boost::numeric::ublas::bounded_vector<T, 3> ac = c - a;
    boost::numeric::ublas::bounded_vector<T, 3> abc = cross(ab, ac);

    if(inner_prod(cross(abc, ac), ao) > T())
    {
        if(inner_prod(ac, ao) > T())
        {
            // [A,C]
            simplexA_.erase(simplexA_.begin() + 1);
            simplexB_.erase(simplexB_.begin() + 1);
            simplexC_.erase(simplexC_.begin() + 1);
            closestPointLine();
        }
        else
        {
            // [A,B]
            simplexA_.erase(simplexA_.begin());
            simplexB_.erase(simplexB_.begin());
            simplexC_.erase(simplexC_.begin());
            closestPointLine();
        }
    }
    else
    {
        if(inner_prod(cross(ab, abc), ao) > T())
        {
            // [A,B]
            simplexA_.erase(simplexA_.begin());
            simplexB_.erase(simplexB_.begin());
            simplexC_.erase(simplexC_.begin());
            closestPointLine();
        }
        else
        {
            // [A,B,C]
            boost::numeric::ublas::bounded_vector<T, 3> origin;
            origin.clear();
            barycentric(a, b, c, origin);
        }
    }
}
template<class T>
void Simplex<T>::closestPointTetrahedron()
{
    boost::numeric::ublas::bounded_vector<T, 3> a = simplexC_[3];
    boost::numeric::ublas::bounded_vector<T, 3> b = simplexC_[2];
    boost::numeric::ublas::bounded_vector<T, 3> c = simplexC_[1];
    boost::numeric::ublas::bounded_vector<T, 3> d = simplexC_[0];
    boost::numeric::ublas::bounded_vector<T, 3> ao = -a;
    boost::numeric::ublas::bounded_vector<T, 3> ab = b - a;
    boost::numeric::ublas::bounded_vector<T, 3> ac = c - a;
    boost::numeric::ublas::bounded_vector<T, 3> ad = d - a;
    boost::numeric::ublas::bounded_vector<T, 3> abc = cross(ab, ac);
    boost::numeric::ublas::bounded_vector<T, 3> acd = cross(ac, ad);
    boost::numeric::ublas::bounded_vector<T, 3> adb = cross(ad, ab);

    if(inner_prod(abc, ao) * inner_prod(abc, ad) < T())
    {
        simplexA_.erase(simplexA_.begin());
        simplexB_.erase(simplexB_.begin());
        simplexC_.erase(simplexC_.begin());
        closestPointTriangle();
    }
    else if(inner_prod(acd, ao) * inner_prod(acd, ab) < T())
    {
        simplexA_.erase(simplexA_.begin() + 2);
        simplexB_.erase(simplexB_.begin() + 2);
        simplexC_.erase(simplexC_.begin() + 2);
        closestPointTriangle();
    }
    else if(inner_prod(adb, ao) * inner_prod(adb, ac) < T())
    {
        simplexA_.erase(simplexA_.begin() + 1);
        simplexB_.erase(simplexB_.begin() + 1);
        simplexC_.erase(simplexC_.begin() + 1);
        closestPointTriangle();
    }
    else
    {
        std::cerr << "Error: unexpected simplex!\n";
        std::cout << "START\n";
        for(int i = 0; i < simplexC_.size(); ++i)
        {
            std::cout << "S: " << simplexC_[i] << std::endl;
        }
        std::cout << "END\n";
    }
}

#endif // SIMPLEX_HPP
