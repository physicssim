/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "rigid_body.hpp"
#include "../objloader.hpp"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>

/*!
 * \class Sphere sphere.hpp "physics/sphere.hpp"
 * \brief The representation of a sphere shape.
 *
 * \tparam T the scalar type.
 */
template<class T>
class Sphere: public RigidBody<T>
{
    friend class boost::serialization::access;
    template<class Archive, class U>
    friend void save_construct_data(Archive &ar, const Sphere<U> *sphere,
        const unsigned int /* file_version */);
public:
    /*!
     * \brief Creates a new sphere with the specified state, mass and radius.
     *
     * \param state the initial state of the sphere.
     * \param mass the mass of the sphere.
     * \param radius the radius of the sphere.
     */
    Sphere(const State<T> &state, const T &mass, const T &radius);
    ~Sphere();
    // Accessors.
    boost::numeric::ublas::bounded_vector<T, 3> getSupport(
        const boost::numeric::ublas::bounded_vector<T, 3> &direction) const;
private:
    template<class Archive>
    void serialize(Archive &ar, const unsigned int /* file_version */)
    {
        ar & boost::serialization::make_nvp("rigid_body",
            boost::serialization::base_object<RigidBody<T> >(*this));
    }

    T radius_;
};

template<class Archive, class U>
inline void save_construct_data(Archive &ar, const Sphere<U> *sphere,
    const unsigned int /* file_version */)
{
    ar << boost::serialization::make_nvp("state", sphere->state_);
    ar << boost::serialization::make_nvp("mass", sphere->mass_);
    ar << boost::serialization::make_nvp("radius", sphere->radius_);
}
template<class Archive, class U>
inline void load_construct_data(Archive &ar, Sphere<U> *sphere,
    const unsigned int /* file_version */)
{
    State<U> state;
    ar >> boost::serialization::make_nvp("state", state);
    U mass;
    ar >> boost::serialization::make_nvp("mass", mass);
    U radius;
    ar >> boost::serialization::make_nvp("radius", radius);

    ::new(sphere)Sphere<U>(state, mass, radius);
}

template<class T>
Sphere<T>::Sphere(const State<T> &state, const T &mass, const T &radius):
    RigidBody<T>(state, mass), radius_(radius)
{
    static const T INERTIA_FACTOR = T(2.0) / T(5.0);
    this->inertia_(0, 0) = radius_ * radius_ * INERTIA_FACTOR * this->mass_;
    this->inertia_(1, 1) = radius_ * radius_ * INERTIA_FACTOR * this->mass_;
    this->inertia_(2, 2) = radius_ * radius_ * INERTIA_FACTOR * this->mass_;

    invert(this->inertia_, this->inverseInertia_);

    if(!ObjLoader().load("data/models/sphere.obj", *this))
    {
        this->indices_.clear();
        this->vertices_.clear();
        this->normals_.clear();
    }

    for(size_t i = 0; i < this->vertices_.size(); ++i)
    {
        this->vertices_[i] *= radius_;
    }
}
template<class T>
Sphere<T>::~Sphere()
{
    // Clean up resources.
}
template<class T>
boost::numeric::ublas::bounded_vector<T, 3> Sphere<T>::getSupport(
    const boost::numeric::ublas::bounded_vector<T, 3> &direction) const
{
    boost::numeric::ublas::bounded_vector<T, 3> v = prod(direction,
        this->getOrientationMatrix());
    if(inner_prod(v, v) <= T())
    {
        boost::numeric::ublas::bounded_vector<T, 3> r;
        r(0) = radius_;
        r(1) = T();
        r(2) = T();

        return r + this->getPosition();
    }
    return prod(this->getOrientationMatrix(), (normalise(v) * radius_)) +
        this->getPosition();
}

#endif // SPHERE_HPP
