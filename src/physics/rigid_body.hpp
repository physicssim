/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef RIGID_BODY_HPP
#define RIGID_BODY_HPP

#include "../boost/invert_matrix.hpp"
#include "../boost/quaternion.hpp"
#include "../boost/vector.hpp"
#include "../math/constants.hpp"
#include "../math/runge_kutta.hpp"
#include "state.hpp"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/interval.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/bind.hpp>
#include <GL/glew.h>

template<class T>
class World;

/*!
 * \class RigidBody rigid_body.hpp "physics/rigid_body.hpp"
 * \brief The representation of a rigid body. This is an abstract class.
 *
 * \tparam T the scalar type.
 */
template<class T>
class RigidBody
{
    friend class World<T>;
    friend class ObjLoader;
    friend class boost::serialization::access;
public:
    virtual ~RigidBody();
    // Accessors.
    //! Returns the inverse inertia of the rigid body in world space.
    boost::numeric::ublas::bounded_matrix<T, 3,
        3> getInverseInertiaWorld() const;
    //! Returns the position of the rigid body.
    const boost::numeric::ublas::bounded_vector<T, 3> & getPosition() const;
    //! Returns the angular momentum of the rigid body.
    const boost::numeric::ublas::bounded_vector<T,
        3> & getAngularMomentum() const;
    //! Returns the orientation matrix of the rigid body.
    boost::numeric::ublas::bounded_matrix<T, 3, 3> getOrientationMatrix() const;
    //! Returns the linear velocity of the rigid body.
    boost::numeric::ublas::bounded_vector<T, 3> getLinearVelocity() const;
    //! Returns the angular velocity of the rigid body.
    boost::numeric::ublas::bounded_vector<T, 3> getAngularVelocity() const;
    //! Returns the total forces acting on the rigid body.
    boost::numeric::ublas::bounded_vector<T, 3> getForce() const;
    //! Returns the total torques acting on the rigid body.
    boost::numeric::ublas::bounded_vector<T, 3> getTorque() const;
    /*!
     * \brief Returns the support point of the rigid body furthest in the
     * requested direction.
     *
     * This function must be implemented for each rigid
     * body type.
     *
     * \param direction the direction in which to search.
     */
    virtual boost::numeric::ublas::bounded_vector<T, 3> getSupport(
        const boost::numeric::ublas::bounded_vector<T, 3> &direction) const = 0;

    //! Apply an external force to the dynamics object.
    void applyForce(const boost::numeric::ublas::bounded_vector<T, 3> &force);

    //! Update the state of the dynamics object.
    void update(const T &time, const T &deltaTime);
    //! Render the rigid body.
    void render();
protected:
    /*!
     * \brief Creates a new rigid body with the specified state and mass.
     *
     * \param state the initial state of the rigid body.
     * \param mass the mass of the rigid body.
     */
    RigidBody(const State<T> &state, const T &mass);

    //! The state of the dynamics object.
    State<T> state_;
    // Constant quantities.
    //! The mass of the rigid body.
    T mass_;
    //! The multiplicative inverse of the mass of the rigid body.
    T inverseMass_;
    //! The local inertia tensor of the rigid body.
    boost::numeric::ublas::bounded_matrix<T, 3, 3> inertia_;
    //! The inverse of the local inertia tensor of the rigid body.
    boost::numeric::ublas::bounded_matrix<T, 3, 3> inverseInertia_;

    std::vector<size_t> indices_;
    std::vector<boost::numeric::ublas::bounded_vector<T, 3> > vertices_;
    std::vector<boost::numeric::ublas::bounded_vector<T, 3> > normals_;
private:
    static RungeKutta<T, State<T> > integrator_;
    void evaluate(const T &time, State<T> &state);

    template<class Archive>
    void serialize(Archive &ar, const unsigned int /* file_version */)
    {
        ar & boost::serialization::make_nvp("force", externalForce_);
        ar & boost::serialization::make_nvp("torque", externalTorque_);
    }

    void computeAABB();

    // External force/torque at time of simulation.
    boost::numeric::ublas::bounded_vector<T, 3> externalForce_;
    boost::numeric::ublas::bounded_vector<T, 3> externalTorque_;
    // Resting contact force/torque. Initially zero, then set before ODE call.
    // Reset to zero for next iteration.
    boost::numeric::ublas::bounded_vector<T, 3> internalForce_;
    boost::numeric::ublas::bounded_vector<T, 3> internalTorque_;
};

template<class T>
RungeKutta<T, State<T> > RigidBody<T>::integrator_;

template<class T>
inline RigidBody<T>::RigidBody(const State<T> &state, const T &mass):
    mass_(mass), state_(state)
{
    //if(mass_ == std::numeric_limits<T>::infinity())
    if(mass_ >= 1e30)
    {
        inverseMass_ = T();
    }
    else
    {
        inverseMass_ = T(1) / mass_;
    }
    inertia_.clear();
    inverseInertia_.clear();

    internalForce_.clear();
    internalTorque_.clear();
}
template<class T>
inline RigidBody<T>::~RigidBody()
{
    //
}
template<class T>
inline boost::numeric::ublas::bounded_matrix<T, 3, 3> RigidBody<T
    >::getInverseInertiaWorld() const
{
    boost::numeric::ublas::bounded_matrix<T, 3, 3> temp = prod(
        getOrientationMatrix(), inverseInertia_);
    return prod(temp, trans(getOrientationMatrix()));
}
template<class T>
inline const boost::numeric::ublas::bounded_vector<T, 3> & RigidBody<T
    >::getPosition() const
{
    return state_.position_;
}
template<class T>
inline const boost::numeric::ublas::bounded_vector<T, 3> & RigidBody<T
    >::getAngularMomentum() const
{
    return state_.angularMomentum_;
}
template<class T>
inline boost::numeric::ublas::bounded_matrix<T, 3, 3> RigidBody<T
    >::getOrientationMatrix() const
{
    return toRotation(state_.orientation_);
}
template<class T>
inline boost::numeric::ublas::bounded_vector<T, 3> RigidBody<T
    >::getLinearVelocity() const
{
    return inverseMass_ * state_.linearMomentum_;
}
template<class T>
inline boost::numeric::ublas::bounded_vector<T, 3> RigidBody<T>::getAngularVelocity() const
{
    return prod(getInverseInertiaWorld(), state_.angularMomentum_);
}
template<class T>
inline boost::numeric::ublas::bounded_vector<T, 3> RigidBody<T>::getForce() const
{
    return externalForce_ + internalForce_;
}
template<class T>
inline boost::numeric::ublas::bounded_vector<T, 3> RigidBody<T>::getTorque() const
{
    return externalTorque_ + internalTorque_;
}
template<class T>
inline void RigidBody<T>::applyForce(
    const boost::numeric::ublas::bounded_vector<T, 3> &force)
{
    externalForce_ = force;
}
template<class T>
void RigidBody<T>::evaluate(const T &/*time*/, State<T> &state)
{
    boost::numeric::ublas::bounded_matrix<T, 3, 3> rotationMatrix = toRotation(
        state.orientation_);
    boost::numeric::ublas::bounded_matrix<T, 3, 3> temp1 = prod(
        rotationMatrix, inverseInertia_);
    boost::numeric::ublas::bounded_matrix<T, 3, 3> temp2 = prod(
        temp1, trans(rotationMatrix));
    boost::numeric::ublas::bounded_vector<T, 3> temp3 = prod(
        temp2, state.angularMomentum_);
    state = State<T>(state.linearMomentum_ * inverseMass_, T(0.5) *
        prod(temp3, state.orientation_), getForce(), getTorque());
}
template<class T>
void RigidBody<T>::update(const T &time, const T &deltaTime)
{
    integrator_(time, deltaTime, state_, boost::bind(&RigidBody::evaluate,
        this, _1, _2));
    state_.orientation_ = normalise(state_.orientation_);
    internalForce_.clear();
    internalTorque_.clear();

    computeAABB();
#if 0
    std::cout << "BEGIN\n";
    std::cout << *state_ << std::endl;
    std::cout << "END\n";
#endif
}
template<class T>
void RigidBody<T>::render()
{
    glPushMatrix();
    glTranslatef(state_.position_(0), state_.position_(1),
        state_.position_(2));
    boost::numeric::ublas::bounded_vector<T, 3> ax = axis(
        state_.orientation_);
    glRotatef(angle(state_.orientation_) * Constants<T>::RAD_TO_DEG,
        ax(0), ax(1), ax(2));
    glBegin(GL_TRIANGLES);
    for(size_t i = 0; i < indices_.size(); ++i)
    {
        glNormal3f(normals_[indices_[i]](0), normals_[indices_[i]](1),
            normals_[indices_[i]](2));
        glVertex3f(vertices_[indices_[i]](0), vertices_[indices_[i]](1),
            vertices_[indices_[i]](2));
    }
    glEnd();
    glPopMatrix();
}
template<class T>
void RigidBody<T>::computeAABB()
{
#if 0
    boost::numeric::ublas::bounded_vector<T, 3> x;
    x(0) = 1; x(1) = 0; x(2) = 0;
    boost::numeric::interval<T> xInterval(inner_prod(x, getSupport(-x)) +
        this->state_.position_(0), inner_prod(x, getSupport(x)) +
        this->state_.position_(0));
    boost::numeric::ublas::bounded_vector<T, 3> y;
    y(0) = 0; y(1) = 1; y(2) = 0;
    boost::numeric::interval<T> yInterval(inner_prod(y, getSupport(-y)) +
        this->state_.position_(1), inner_prod(y, getSupport(y)) +
        this->state_.position_(1));
    boost::numeric::ublas::bounded_vector<T, 3> z;
    z(0) = 0; z(1) = 0; z(2) = 1; 
    boost::numeric::interval<T> zInterval(inner_prod(z, getSupport(-z)) +
        this->state_.position_(2), inner_prod(z, getSupport(z)) +
        this->state_.position_(2));
#endif
#if 0
    std::cout << "[" << lower(xInterval) << ", " << upper(xInterval) << "]" << std::endl;
    std::cout << "[" << lower(yInterval) << ", " << upper(yInterval) << "]" << std::endl;
    std::cout << "[" << lower(zInterval) << ", " << upper(zInterval) << "]" << std::endl;
#endif
}

#endif // RIGID_BODY_HPP
