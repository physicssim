/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BOX_HPP
#define BOX_HPP

#include "rigid_body.hpp"
#include "../objloader.hpp"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>

/*!
 * \class Box box.hpp "physics/box.hpp"
 * \brief The representation of a box shape.
 *
 * \tparam T the scalar type.
 */
template<class T>
class Box: public RigidBody<T>
{
    friend class boost::serialization::access;
    template<class Archive, class U>
    friend void save_construct_data(Archive &ar, const Box<U> *box,
        const unsigned int /* file_version */);
public:
    /*!
     * \brief Creates a new box with the specified state, mass and
     * half-extents.
     *
     * \param state the initial state of the box.
     * \param mass the mass of the box.
     * \param halfExtents the half-extents of the box.
     */
    Box(const State<T> &state, const T &mass,
        const boost::numeric::ublas::bounded_vector<T, 3> &halfExtents);
    ~Box();
    // Accessors.
    boost::numeric::ublas::bounded_vector<T, 3> getSupport(
        const boost::numeric::ublas::bounded_vector<T, 3> &direction) const;
private:
    template<class Archive>
    void serialize(Archive &ar, const unsigned int /* file_version */)
    {
        ar & boost::serialization::make_nvp("rigid_body",
            boost::serialization::base_object<RigidBody<T> >(*this));
    }

    boost::numeric::ublas::bounded_vector<T, 3> halfExtents_;
};

template<class Archive, class U>
inline void save_construct_data(Archive &ar, const Box<U> *box,
    const unsigned int /* file_version */)
{
    ar << boost::serialization::make_nvp("state", box->state_);
    ar << boost::serialization::make_nvp("mass", box->mass_);
    ar << boost::serialization::make_nvp("half_extents", box->halfExtents_);
}
template<class Archive, class U>
inline void load_construct_data(Archive &ar, Box<U> *box,
    const unsigned int /* file_version */)
{
    State<U> state;
    ar >> boost::serialization::make_nvp("state", state);
    U mass;
    ar >> boost::serialization::make_nvp("mass", mass);
    boost::numeric::ublas::bounded_vector<U, 3> halfExtents;
    ar >> boost::serialization::make_nvp("half_extents", halfExtents);

    ::new(box)Box<U>(state, mass, halfExtents);
}

template<class T>
Box<T>::Box(const State<T> &state, const T &mass,
    const boost::numeric::ublas::bounded_vector<T, 3> &halfExtents):
    RigidBody<T>(state, mass), halfExtents_(halfExtents)
{
    static const T INERTIA_FACTOR = T(1.0) / T(3.0);
    this->inertia_(0, 0) = (halfExtents_(0) * halfExtents_(0) +
        halfExtents_(2) * halfExtents_(2)) * INERTIA_FACTOR * this->mass_;
    this->inertia_(1, 1) = (halfExtents_(1) * halfExtents_(1) +
        halfExtents_(2) * halfExtents_(2)) * INERTIA_FACTOR * this->mass_;
    this->inertia_(2, 2) = (halfExtents_(0) * halfExtents_(0) +
        halfExtents_(1) * halfExtents_(1)) * INERTIA_FACTOR * this->mass_;
    
    invert(this->inertia_, this->inverseInertia_);

    if(!ObjLoader().load("data/models/box.obj", *this))
    {
        this->indices_.clear();
        this->vertices_.clear();
        this->normals_.clear();
    }

    for(size_t i = 0; i < this->vertices_.size(); ++i)
    {
        this->vertices_[i] = element_prod(this->vertices_[i], halfExtents_);
    }
}
template<class T>
Box<T>::~Box()
{
    //
}
template<class T>
boost::numeric::ublas::bounded_vector<T, 3> Box<T>::getSupport(
    const boost::numeric::ublas::bounded_vector<T, 3> &direction) const
{
    boost::numeric::ublas::bounded_vector<T, 3> support;
    boost::numeric::ublas::bounded_vector<T, 3> v = prod(direction,
        this->getOrientationMatrix());

    support(0) = v(0) < T() ? -halfExtents_(0) : halfExtents_(0);
    support(1) = v(1) < T() ? -halfExtents_(1) : halfExtents_(1);
    support(2) = v(2) < T() ? -halfExtents_(2) : halfExtents_(2);

    return prod(this->getOrientationMatrix(), support) +
        this->getPosition();
}

#endif // BOX_HPP
