/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef STATE_HPP
#define STATE_HPP

#include <boost/math/quaternion.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/operators.hpp>
#include <iostream>

template<class T>
class RigidBody;

/*!
 * \class State state.hpp "physics/state.hpp"
 * \brief The state of a dynamics object.
 *
 * \tparam T the scalar type.
 */
template<class T>
class State: boost::addable<State<T>, boost::subtractable<State<T>,
    boost::multipliable<State<T>, T> > >
{
    friend class RigidBody<T>;
    friend class boost::serialization::access;
    template<class Archive, class U>
    friend void save_construct_data(Archive &ar, const State<U> *state,
        const unsigned int /* file_version */);
public:
    /*!
     * \brief Creates a new state with a zero vector for position,
     * linear momentum and angular momentum. The orientation is a unit
     * quaternion.
     */
    State();
    /*!
     * \brief Creates a new state with the specified position, orientation,
     * linear momentum and angular momentum.
     *
     * \param position the initial position.
     * \param orientation the initial orientation.
     * \param linearMomentum the initial linear momentum.
     * \param angularMomentum the initial angular momentum.
     */
    State(const boost::numeric::ublas::bounded_vector<T, 3> &position,
        const boost::math::quaternion<T> &orientation,
        const boost::numeric::ublas::bounded_vector<T, 3> &linearMomentum,
        const boost::numeric::ublas::bounded_vector<T, 3> &angularMomentum);
    ~State();
    //! The copy constructor.
    State(const State &s);
    //! The assignment operator.
    State &operator=(const State &s);
    //! The operator +=.
    State &operator+=(const State &s);
    //! The operator -=.
    State &operator-=(const State &s);
    //! Multiply the state by a scalar.
    State &operator*=(const T &t);

    /*!
     * \brief Apply an impulse at the specified position.
     *
     * \param position the position at which to apply the impulse.
     * \param impulse the impulse to apply.
     */
    void applyImpulse(const boost::numeric::ublas::bounded_vector<T,
        3> &position, const boost::numeric::ublas::bounded_vector<T,
        3> &impulse);
    
    /*!
     * \brief Write the state to the output stream.
     *
     * \tparam U the scalar type.
     * \param os the output stream.
     * \param state the state to write.
     */
    template<class U>
    friend std::ostream &operator<<(std::ostream &os, const State<U> &state);

private:
    template<class Archive>
    void serialize(Archive &ar, const unsigned int /* file_version */)
    {
        ar & boost::serialization::make_nvp("position", position_);
        ar & boost::serialization::make_nvp("orientation", orientation_);
        ar & boost::serialization::make_nvp("linear_momentum",
            linearMomentum_);
        ar & boost::serialization::make_nvp("angular_momentum",
            angularMomentum_);
    }

    boost::numeric::ublas::bounded_vector<T, 3> position_;
    boost::math::quaternion<T> orientation_;
    boost::numeric::ublas::bounded_vector<T, 3> linearMomentum_;
    boost::numeric::ublas::bounded_vector<T, 3> angularMomentum_;
};

template<class U>
std::ostream &operator<<(std::ostream &os, const State<U> &state)
{
    os << "position: " << state.position_ << std::endl;
    os << "orientation: " << state.orientation_ << std::endl;
    os << "linear momentum: " << state.linearMomentum_ << std::endl;
    os << "angular momentum: " << state.angularMomentum_;

    return os;
}

template<class T>
State<T>::State()
{
    position_.clear();
    orientation_ = boost::math::quaternion<T>(T(1));
    linearMomentum_.clear();
    angularMomentum_.clear();
}
template<class T>
State<T>::State(const boost::numeric::ublas::bounded_vector<T, 3> &position,
    const boost::math::quaternion<T> &orientation,
    const boost::numeric::ublas::bounded_vector<T, 3> &linearMomentum,
    const boost::numeric::ublas::bounded_vector<T, 3> &angularMomentum):
    position_(position), orientation_(orientation), linearMomentum_(
    linearMomentum), angularMomentum_(angularMomentum)
{
    //
}
template<class T>
State<T>::~State()
{
}
template<class T>
State<T>::State(const State<T> &s)
{
    position_ = s.position_;
    orientation_ = s.orientation_;
    linearMomentum_ = s.linearMomentum_;
    angularMomentum_ = s.angularMomentum_;
}
template<class T>
State<T> &State<T>::operator=(const State<T> &s)
{
    position_ = s.position_;
    orientation_ = s.orientation_;
    linearMomentum_ = s.linearMomentum_;
    angularMomentum_ = s.angularMomentum_;

    return *this;
}
template<class T>
State<T> &State<T>::operator+=(const State<T> &s)
{
    position_ += s.position_;
    orientation_ += s.orientation_;
    linearMomentum_ += s.linearMomentum_;
    angularMomentum_ += s.angularMomentum_;

    return *this;
}
template<class T>
State<T> &State<T>::operator-=(const State<T> &s)
{
    position_ -= s.position_;
    orientation_ -= s.orientation_;
    linearMomentum_ -= s.linearMomentum_;
    angularMomentum_ -= s.angularMomentum_;

    return *this;
}
template<class T>
State<T> &State<T>::operator*=(const T &t)
{
    position_ *= t;
    orientation_ *= t;
    linearMomentum_ *= t;
    angularMomentum_ *= t;

    return *this;
}
template<class T>
void State<T>::applyImpulse(const boost::numeric::ublas::bounded_vector<T,
    3> &position, const boost::numeric::ublas::bounded_vector<T, 3> &impulse)
{
    linearMomentum_ += impulse;
    angularMomentum_ += cross(position, impulse);
}

#endif // STATE_HPP
