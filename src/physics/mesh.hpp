/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef MESH_HPP
#define MESH_HPP

#include "rigid_body.hpp"
#include "../util/proximity_list.hpp"
#include "../objloader.hpp"
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>
#include <string>

/*!
 * \class Mesh mesh.hpp "physics/mesh.hpp"
 * \brief The representation of a polytope shape.
 *
 * \tparam T the scalar type.
 */
template<class T>
class Mesh: public RigidBody<T>
{
    friend class boost::serialization::access;
    template<class Archive, class U>
    friend void save_construct_data(Archive &ar, const Mesh<U> *mesh,
        const unsigned int /* file_version */);
public:
    Mesh(const State<T> &state, const T &mass, const std::string &model);
    ~Mesh();
    // Accessors.
    boost::numeric::ublas::bounded_vector<T, 3> getSupport(
        const boost::numeric::ublas::bounded_vector<T, 3> &direction) const;
private:
    template<class Archive>
    void serialize(Archive &ar, const unsigned int /* file_version */)
    {
        ar & boost::serialization::make_nvp("rigid_body",
            boost::serialization::base_object<RigidBody<T> >(*this));
    }

    struct SquaredDistance
    {
        typedef T result_type;

        result_type operator()(
            const boost::numeric::ublas::bounded_vector<T, 3> &left,
            const boost::numeric::ublas::bounded_vector<T, 3> &right) const
        {
            return inner_prod(left - right, left - right);
        }
    };

    std::string model_;
    proximity_list<boost::numeric::ublas::bounded_vector<T, 3>,
        SquaredDistance> list_;
};

template<class Archive, class U>
inline void save_construct_data(Archive &ar, const Mesh<U> *mesh,
    const unsigned int /* file_version */)
{
    ar << boost::serialization::make_nvp("state", mesh->state_);
    ar << boost::serialization::make_nvp("mass", mesh->mass_);
    ar << boost::serialization::make_nvp("model", mesh->model_);
}
template<class Archive, class U>
inline void load_construct_data(Archive &ar, Mesh<U> *mesh,
    const unsigned int /* file_version */)
{
    State<U> state;
    ar >> boost::serialization::make_nvp("state", state);
    U mass;
    ar >> boost::serialization::make_nvp("mass", mass);
    std::string model;
    ar >> boost::serialization::make_nvp("model", model);

    ::new(mesh)Mesh<U>(state, mass, model);
}

template<class T>
Mesh<T>::Mesh(const State<T> &state, const T &mass, const std::string &model):
    RigidBody<T>(state, mass), model_(model)
{
    if(!ObjLoader().load("data/models/" + model, *this))
    {
        this->indices_.clear();
        this->vertices_.clear();
        this->normals_.clear();
    }
    list_.insert(this->vertices_.begin(), this->vertices_.end());
}
template<class T>
Mesh<T>::~Mesh()
{
    // Clean up resources.
}
template<class T>
boost::numeric::ublas::bounded_vector<T, 3> Mesh<T>::getSupport(
    const boost::numeric::ublas::bounded_vector<T, 3> &direction) const
{
    boost::numeric::ublas::bounded_vector<T, 3> v = prod(direction,
        this->getOrientationMatrix());
    if(inner_prod(v, v) <= T())
    {
        return *list_.begin() + this->getPosition();
    }

    return prod(this->getOrientationMatrix(), *list_.find_nearest(v)) +
        this->getPosition();
}

#endif // MESH_HPP
