/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2007 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef GJK_HPP
#define GJK_HPP

#include "simplex.hpp"
#include "rigid_body.hpp"
#include "../boost/vector.hpp"
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

template<class T>
class GJK
{
public:
    T intersect(const RigidBody<T> &first, const RigidBody<T> &second);
    boost::numeric::ublas::bounded_vector<T, 3> getContactPoint() const;
    boost::numeric::ublas::bounded_vector<T, 3> getContactNormal() const;
private:
    Simplex<T> simplex_;
};

// TODO: Make intersection test more robust when dealing with floats rather
// than doubles.
template<class T>
T GJK<T>::intersect(const RigidBody<T> &first, const RigidBody<T> &second)
{
    // GJK distance algorithm.
    simplex_.clear();

    boost::numeric::ublas::bounded_vector<T, 3> start;
    start(0) = 1.0;
    start(1) = 0.0;
    start(2) = 0.0;

    boost::numeric::ublas::bounded_vector<T, 3> p = first.getSupport(-start);
    boost::numeric::ublas::bounded_vector<T, 3> q = second.getSupport(start);
    boost::numeric::ublas::bounded_vector<T, 3> v = p - q;

    boost::numeric::ublas::bounded_vector<T, 3> w;

    T delta = T();
    bool done = false;
    unsigned int count = 20;
    while(!done)
    {
        if(!count)
        {
            break;
        }
        --count;
        p = first.getSupport(-v);
        q = second.getSupport(v);
        w = p - q;

        delta = std::max(inner_prod(v, w) / norm_2(v), delta);

        done = norm_2(v) - delta <= norm_2(v) * 1e-3 || norm_2(v) <= 0.5;
        if(!done)
        {
            simplex_.addPoint(p, q);
            simplex_.getClosest();
            v = simplex_.closestPointC();
        }
    }
    return norm_2(v);
}
template<class T>
inline boost::numeric::ublas::bounded_vector<T, 3> GJK<T>::getContactPoint() const
{
    return (simplex_.closestPointA() + simplex_.closestPointB()) * T(0.5);
}
template<class T>
inline boost::numeric::ublas::bounded_vector<T, 3> GJK<T>::getContactNormal() const
{
    return normalise(simplex_.closestPointA() - simplex_.closestPointB());
}

#endif // GJK_HPP
