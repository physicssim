/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifdef __WIN32__
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include <boost/archive/xml_iarchive.hpp>
#include <boost/program_options.hpp>
#include <cmath>
#include <iostream>
#include <string>
using std::cout;

namespace po = boost::program_options;

#include <GL/glew.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "world.hpp"
#include "math/constants.hpp"

typedef double T;

static const T TIME_STEP = T(0.01);
static const T FREQUENCY = T(1) / TIME_STEP;

T simulationTime_ = 0;
T previousTime_;
long frameCount_;
T totalTime_ = 0;
T framesPerSecond_ = 0;

bool quit_ = false;

static const int WINDOW_WIDTH = 800;
static const int WINDOW_HEIGHT = 600;

TTF_Font * font_ = 0;

bool movingLeft_ = false;
bool movingRight_ = false;
bool movingIn_ = false;
bool movingOut_ = false;

World<T> *world_ = 0;

static bool initialise()
{
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
    {
        std::cerr << "SDL_Init: " << SDL_GetError() << std::endl;

        return false;
    }

    previousTime_ = SDL_GetTicks();

    if(TTF_Init() < 0)
    {
        std::cerr << "TTF_Init: " << TTF_GetError() << std::endl;

        return false;
    }

    font_ = TTF_OpenFont("data/fonts/DejaVuSansMono.ttf", 18);

    if(const SDL_VideoInfo *video = SDL_GetVideoInfo())
    {
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

        if(!SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT,
            video->vfmt->BitsPerPixel, SDL_OPENGL))
        {
            std::cerr << "SDL_SetVideoMode: " << SDL_GetError() << std::endl;

            return false;
        }
    }

    GLenum error = glewInit();
    if(error != GLEW_OK)
    {
        std::cerr << "GLEW Init failed: " << glewGetErrorString(error) <<
            std::endl;

        return false;
    }

    return true;
}

static void initialiseGL()
{
    glClearColor(1.0f, 1.0f, 0.0f, 0.0f);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_CULL_FACE);

    glTranslatef(0.0f, 10.0f, -130.0f);
    glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
}
static void setViewportSize(const int &width, const int &height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    float aspect = width / static_cast<float>(height);
    gluPerspective(45, aspect, 0.1f, 300.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

static void cleanup()
{
    TTF_CloseFont(font_);
    TTF_Quit();
    SDL_Quit();
}

static int nextPowerOfTwo(const int &number)
{
    return static_cast<int>(std::pow(2, std::ceil(std::log(T(number)) *
        Constants<T>::INV_LN_2)));
}
static void renderText(const int &x, const int &y, const std::string &text)
{
    if(!font_)
    {
        return;
    }

    SDL_Color colour = {255, 255, 255, 0};

    // There is a bug in freetype version 2.2.1 (debian/ubuntu) that returns
    // an error when rendering spaces in mono mode. Seems to be fixed in later
    // versions.
    SDL_Surface *rgbSurface = 0;
    int w;
    int h;
    if(SDL_Surface *initial = TTF_RenderText_Solid(font_, text.c_str(),
        colour))
    {
        if(GLEW_ARB_texture_non_power_of_two || GLEW_VERSION_2_0)
        {
            w = initial->w;
            h = initial->h;
        }
        else
        {
            w = nextPowerOfTwo(initial->w);
            h = nextPowerOfTwo(initial->h);
        }
        rgbSurface = SDL_CreateRGBSurface(0, w, h,
            SDL_GetVideoSurface()->format->BitsPerPixel, 0x00ff0000,
            0x0000ff00, 0x000000ff, 0xff000000);
        SDL_BlitSurface(initial, 0, rgbSurface, 0);
        SDL_FreeSurface(initial);
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    SDL_LockSurface(rgbSurface);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE,
        rgbSurface->pixels);
    SDL_UnlockSurface(rgbSurface);

    glPushAttrib(GL_TEXTURE_BIT | GL_CURRENT_BIT | GL_PIXEL_MODE_BIT);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glColor3f(0.0f, 0.5f, 1.0f);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 1.0f); 
    glVertex2i(x, y);
    glTexCoord2f(1.0f, 1.0f); 
    glVertex2i(x + w, y);
    glTexCoord2f(1.0f, 0.0f); 
    glVertex2i(x + w, y + h);
    glTexCoord2f(0.0f, 0.0f); 
    glVertex2i(x, y + h);
    glEnd();
    
    glPopAttrib();
    SDL_FreeSurface(rgbSurface);
    glDeleteTextures(1, &texture);
}

static void displayStatistics()
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    gluOrtho2D(0.0, viewport[2], 0.0, viewport[3]);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    std::ostringstream stats;
    stats.setf(std::ios_base::fixed, std::ios_base::floatfield);
    stats.precision(2);
    stats << "FPS:" << framesPerSecond_ << ", Time:" << simulationTime_ *
        TIME_STEP;
    renderText(10, 10, stats.str());

    glPopAttrib();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

static void update()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    long currentTime = SDL_GetTicks();
    long deltaTime = currentTime - previousTime_;

    T timeCounter = deltaTime * T(0.001) * FREQUENCY;

    int count = static_cast<int>(std::floor(simulationTime_ +
        timeCounter) - std::floor(simulationTime_));
    while(count)
    {
        world_->update(simulationTime_, TIME_STEP);

        --count;
    }
    simulationTime_ += timeCounter;

    totalTime_ += deltaTime * T(0.001);
    ++frameCount_;

    // Render world.
    world_->render();

    framesPerSecond_ = frameCount_ / totalTime_;

    if(frameCount_ > 1000)
    {
        frameCount_ = 0;
        totalTime_ = 0;
    }

    displayStatistics();

    previousTime_ = currentTime;
}

static void run()
{
    SDL_Event event;
    while(!quit_)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    quit_ = true;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE:
                            quit_ = true;
                            break;
                        case SDLK_w:
                            movingIn_ = true;
                            break;
                        case SDLK_s:
                            movingOut_ = true;
                            break;
                        case SDLK_a:
                            movingLeft_ = true;
                            break;
                        case SDLK_d:
                            movingRight_ = true;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        SDL_Delay(1);
        update();
        SDL_GL_SwapBuffers();
    }
}

#include "physics/box.hpp"
#include "physics/mesh.hpp"
#include "physics/sphere.hpp"

int main(int argc, char **argv)
{
    po::options_description description("Usage: physicssim [options]\n"
        "Options");
    description.add_options()
        ("help", "Display this help message")
        ("scene", po::value<std::string>(), "Open scene file");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, description), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
        cout << description << "\n";
        return 1;
    }

    if(!initialise())
    {
        cleanup();
        return 1;
    }
    
    std::ifstream ifs;
    if(vm.count("scene"))
    {
        ifs.open(("data/scenes/" + vm["scene"].as<std::string>()).c_str());
    }
    else
    {
        ifs.open("data/scenes/basic.xml");
    }
    
    if(!ifs)
    {
        cleanup();
        std::cerr << "Error: failed to open scene!\n";
        return 1;
    }

    boost::archive::xml_iarchive ia(ifs);
    ia.register_type<Box<T> >();
    ia.register_type<Mesh<T> >();
    ia.register_type<Sphere<T> >();

    ia & boost::serialization::make_nvp("world", world_);

    setViewportSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    initialiseGL();
    run();

    cleanup();
    return 0;
}
