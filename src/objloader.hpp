/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef OBJ_LOADER_HPP
#define OBJ_LOADER_HPP

#include "meshloader.hpp"
#include <boost/numeric/ublas/vector.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <iostream>
#include <fstream>
#include <vector>

class ObjLoader: public MeshLoader
{
public:
    virtual bool load(const std::string &filename, RigidBody<T> &body);
private:
    typedef boost::tokenizer<boost::char_separator<char> > Tokeniser;
    static const boost::char_separator<char> DELIMITERS;
};

const boost::char_separator<char> ObjLoader::DELIMITERS(" /\r\n");

bool ObjLoader::load(const std::string &filename, RigidBody<T> &body)
{
    std::ifstream file;
    file.open(filename.c_str());

    if(!file)
    {
        std::cerr << "Error opening file: " << filename << std::endl;
        return false;
    }

    std::string buffer;
    std::string identifier;

    while(getline(file, buffer))
    {
        Tokeniser tokens(buffer, DELIMITERS);
        Tokeniser::const_iterator iter = tokens.begin();

        if(iter != tokens.end())
        {
            identifier = (* iter);
            ++iter;

            if(identifier == "v")
            {
                boost::numeric::ublas::bounded_vector<T, 3> vertex;
                vertex(0) = boost::lexical_cast<T>(*iter);
                ++iter;
                vertex(1) = boost::lexical_cast<T>(*iter);
                ++iter;
                vertex(2) = boost::lexical_cast<T>(*iter);
                ++iter;
                body.vertices_.push_back(vertex);
            }
            else if(identifier == "vn")
            {
                boost::numeric::ublas::bounded_vector<T, 3> normal;
                normal(0) = boost::lexical_cast<T>(*iter);
                ++iter;
                normal(1) = boost::lexical_cast<T>(*iter);
                ++iter;
                normal(2) = boost::lexical_cast<T>(*iter);
                ++iter;
                body.normals_.push_back(normal);
            }
            else if(identifier == "f")
            {
                // TODO: Fix to read all indices in format.
                body.indices_.push_back(boost::lexical_cast<size_t>(*iter) - 1);
                ++iter;
                ++iter;
                body.indices_.push_back(boost::lexical_cast<size_t>(*iter) - 1);
                ++iter;
                ++iter;
                body.indices_.push_back(boost::lexical_cast<size_t>(*iter) - 1);
            }
        }
    }

    file.close();

    return true;
}

#endif // OBJ_LOADER_HPP
