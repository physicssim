/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef WORLD_HPP
#define WORLD_HPP

#include "math/lemke.hpp"
#include "physics/gjk.hpp"
#include "physics/rigid_body.hpp"
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <vector>

template<class T>
class World
{
    friend class boost::serialization::access;
public:
    struct Contact
    {
        Contact(RigidBody<T> &first, RigidBody<T> &second,
            const boost::numeric::ublas::bounded_vector<T, 3> &point,
            const boost::numeric::ublas::bounded_vector<T, 3> &normal):
            first_(first), second_(second), point_(point), normal_(normal)
        {
            //
        }
        Contact(const Contact &contact): first_(contact.first_),
            second_(contact.second_), point_(contact.point_),
            normal_(contact.normal_)
        {
            //
        }
        Contact &operator=(const Contact &contact)
        {
            first_ = contact.first_;
            second_ = contact.second_;
            point_ = contact.point_;
            normal_ = contact.normal_;

            return *this;
        }
        RigidBody<T> &first_;
        RigidBody<T> &second_;
        boost::numeric::ublas::bounded_vector<T, 3> point_;
        boost::numeric::ublas::bounded_vector<T, 3> normal_;
    };

    World()
    {
    }
    ~World()
    {
        clear();
    }
    void clear()
    {
        for(size_t i = 0; i < objects_.size(); ++i)
        {
            delete objects_[i];
        }
        objects_.clear();
    }
    void update(const T &time, const T &deltaTime)
    {
        contacts_.clear();
        for(typename std::vector<RigidBody<T> *>::iterator first =
            objects_.begin(); first != objects_.end(); ++first)
        {
            for(typename std::vector<RigidBody<T> *>::iterator second = first;
                second != objects_.end(); ++second)
            {
                if(first != second)
                {
                    T distance2 = gjk_.intersect(**first, **second);
                    if(distance2 <= T(1.0))
                    {
                        contacts_.push_back(Contact(**first, **second,
                            gjk_.getContactPoint(), gjk_.getContactNormal()));
                    }
                }
            }
        }

        // Compute LCP matrix.
        boost::numeric::ublas::matrix<T> lcpMatrix(contacts_.size(),
            contacts_.size());
        // Compute pre-impulse velocity vector.
        boost::numeric::ublas::vector<T> preImpulseVelocity(contacts_.size());
        // Compute resting contact vector.
        boost::numeric::ublas::vector<T> restingContact(contacts_.size());

        boost::numeric::ublas::vector<T> impulseMag(contacts_.size());
        for(size_t i = 0; i < contacts_.size(); ++i)
        {
            const Contact &ci = contacts_[i];

            boost::numeric::ublas::bounded_vector<T, 3> rAi = ci.point_ -
                ci.first_.getPosition();
            boost::numeric::ublas::bounded_vector<T, 3> rBi = ci.point_ -
                ci.second_.getPosition();
            boost::numeric::ublas::bounded_vector<T, 3> velA =
                ci.first_.getLinearVelocity() + cross(
                ci.first_.getAngularVelocity(), rAi);
            boost::numeric::ublas::bounded_vector<T, 3> velB =
                ci.second_.getLinearVelocity() + cross(
                ci.second_.getAngularVelocity(), rBi);

            preImpulseVelocity(i) = inner_prod(ci.normal_, velA - velB);

            boost::numeric::ublas::bounded_vector<T, 3> rANi = cross(rAi,
                ci.normal_);
            boost::numeric::ublas::bounded_vector<T, 3> rBNi = cross(rBi,
                ci.normal_);

            for(size_t j = 0; j < contacts_.size(); ++j)
            {
                const Contact &cj = contacts_[j];
                boost::numeric::ublas::bounded_vector<T, 3> rAj = cj.point_ -
                    cj.first_.getPosition();
                boost::numeric::ublas::bounded_vector<T, 3> rBj = cj.point_ -
                    cj.second_.getPosition();
                boost::numeric::ublas::bounded_vector<T, 3> rANj = cross(rAj,
                    cj.normal_);
                boost::numeric::ublas::bounded_vector<T, 3> rBNj = cross(rBj,
                    cj.normal_);

                lcpMatrix(i, j) = T();

                if(&ci.first_ == &cj.first_)
                {
                    lcpMatrix(i, j) += ci.first_.inverseMass_ *
                        inner_prod(ci.normal_, cj.normal_);
                    lcpMatrix(i, j) += inner_prod(rANi,
                        prod(ci.first_.getInverseInertiaWorld(), rANj));
                }
                else if(&ci.first_ == &cj.second_)
                {
                    lcpMatrix(i, j) -= ci.first_.inverseMass_ *
                        inner_prod(ci.normal_, cj.normal_);
                    lcpMatrix(i, j) -= inner_prod(rANi,
                        prod(ci.first_.getInverseInertiaWorld(), rANj));
                }

                if(&ci.second_ == &cj.first_)
                {
                    lcpMatrix(i, j) -= ci.second_.inverseMass_ *
                        inner_prod(ci.normal_, cj.normal_);
                    lcpMatrix(i, j) -= inner_prod(rBNi,
                        prod(ci.second_.getInverseInertiaWorld(), rBNj));
                }
                else if(&ci.second_ == &cj.second_)
                {
                    lcpMatrix(i, j) += ci.second_.inverseMass_ *
                        inner_prod(ci.normal_, cj.normal_);
                    lcpMatrix(i, j) += inner_prod(rBNi,
                        prod(ci.second_.getInverseInertiaWorld(), rBNj));
                }
            }

            // Resting contact vector.
            boost::numeric::ublas::bounded_vector<T, 3> wAxrAi = cross(
                ci.first_.getAngularVelocity(), rAi);
            boost::numeric::ublas::bounded_vector<T, 3> At1 =
                ci.first_.inverseMass_ * ci.first_.getForce();
            boost::numeric::ublas::bounded_vector<T, 3> At2 = cross(
                prod(ci.first_.getInverseInertiaWorld(),
                ci.first_.getTorque() + cross(
                ci.first_.getAngularMomentum(),
                ci.first_.getAngularVelocity())), rAi);
            boost::numeric::ublas::bounded_vector<T, 3> At3 = cross(
                ci.first_.getAngularVelocity(), wAxrAi);
            boost::numeric::ublas::bounded_vector<T, 3> At4 =
                ci.first_.getLinearVelocity() + wAxrAi;

            boost::numeric::ublas::bounded_vector<T, 3> wBxrBi = cross(
                ci.second_.getAngularVelocity(), rBi);
            boost::numeric::ublas::bounded_vector<T, 3> Bt1 =
                ci.second_.inverseMass_ * ci.second_.getForce();
            boost::numeric::ublas::bounded_vector<T, 3> Bt2 = cross(
                prod(ci.second_.getInverseInertiaWorld(),
                ci.second_.getTorque() + cross(
                ci.second_.getAngularMomentum(),
                ci.second_.getAngularVelocity())), rBi);
            boost::numeric::ublas::bounded_vector<T, 3> Bt3 = cross(
                ci.second_.getAngularVelocity(), wBxrBi);
            boost::numeric::ublas::bounded_vector<T, 3> Bt4 =
                ci.second_.getLinearVelocity() + wBxrBi;

            boost::numeric::ublas::bounded_vector<T, 3> Ndot =
                cross(ci.second_.getAngularVelocity(), ci.normal_);

            restingContact(i) = inner_prod(ci.normal_, At1 + At2 + At3 -
                Bt1 - Bt2 - Bt3) + T(2) * inner_prod(Ndot, At4 - Bt4);
        }
        if(!contacts_.empty())
        {
            boost::numeric::ublas::vector<T> postImpulseVelocity(contacts_.size());
            if(!Lemke<T>()(preImpulseVelocity, lcpMatrix, postImpulseVelocity,
                impulseMag))
            {
                std::cerr << "################ Ray termination1!\n";
                postImpulseVelocity.clear();
                impulseMag.clear();
            }
#if 1
            std::cout << "pre v: " << preImpulseVelocity << std::endl;
            std::cout << "m: " << lcpMatrix << std::endl;
            std::cout << "imp: " << impulseMag << std::endl;
            std::cout << "post v: " << postImpulseVelocity << std::endl;
#endif
            for(size_t i = 0; i < contacts_.size(); ++i)
            {
                const Contact &ci = contacts_[i];
                boost::numeric::ublas::bounded_vector<T, 3> rA = ci.point_ -
                    ci.first_.getPosition();
                boost::numeric::ublas::bounded_vector<T, 3> rB = ci.point_ -
                    ci.second_.getPosition();

                boost::numeric::ublas::bounded_vector<T, 3> impulse =
                    impulseMag(i) * ci.normal_;

                ci.first_.state_.applyImpulse(rA, impulse);
                ci.second_.state_.applyImpulse(rB, -impulse);
            }

            boost::numeric::ublas::vector<T> relAcc(contacts_.size());
            boost::numeric::ublas::vector<T> restingMag(contacts_.size());
            if(!Lemke<T>()(restingContact, lcpMatrix, relAcc, restingMag))
            {
                std::cerr << "################ Ray termination2!\n";
                relAcc.clear();
                restingMag.clear();
            }
#if 0
            std::cout << "pre v: " << restingContact<< std::endl;
            std::cout << "m: " << lcpMatrix << std::endl;
            std::cout << "rel acc: " << relAcc << std::endl;
            std::cout << "resting Mag: " << restingMag << std::endl;
#endif
            for(size_t i = 0; i < contacts_.size(); ++i)
            {
                const Contact &ci = contacts_[i];
                boost::numeric::ublas::bounded_vector<T, 3> rA = ci.point_ -
                    ci.first_.getPosition();
                boost::numeric::ublas::bounded_vector<T, 3> rB = ci.point_ -
                    ci.second_.getPosition();

                boost::numeric::ublas::bounded_vector<T, 3> resting =
                    restingMag(i) * ci.normal_;

                // Resting contact forces.
                ci.first_.internalForce_ += resting;
                ci.second_.internalForce_ -= resting;
                // Resting contact torques.
                ci.first_.internalTorque_ += cross(rA, resting);
                ci.second_.internalTorque_ -= cross(rB, resting);
            }
        }
        std::for_each(objects_.begin(), objects_.end(),
            boost::bind(&RigidBody<T>::update, _1, time, deltaTime));
    }
    void render() const
    {
        std::for_each(objects_.begin(), objects_.end(), boost::bind(
            &RigidBody<T>::render, _1));
    }
public:
    template<class Archive>
    void serialize(Archive &ar, const unsigned int /* file_version */)
    {
        ar & boost::serialization::make_nvp("objects", objects_);
    }

    boost::numeric::ublas::bounded_vector<T, 3> gravity_;
    std::vector<RigidBody<T> *> objects_;
    GJK<T> gjk_;
    std::vector<Contact> contacts_;
};

#endif // WORLD_HPP
