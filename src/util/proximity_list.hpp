#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/iterator/iterator_concepts.hpp>
#include <boost/iterator/reverse_iterator.hpp>
#include <boost/type_traits/is_convertible.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/concept_check.hpp>
#include <boost/operators.hpp>
#include <boost/random.hpp>
#include <algorithm>
#include <cmath>
#include <set>

namespace
{
    boost::mt19937 rng;
}

/*!
 * \class proximity_list proximity_list.hpp "util/proximity_list.hpp"
 * \brief The triangle inequality nearest neighbour list for acceleration of
 *        nearest neighbour queries.
 *
 * Implementation of the algorithms as described in C. Shu, M. Greenspan, G.
 * Godin, "Nearest Neighbor Search Through Function Minimization", 13th
 * Canadian Conference on Computational Geometry,  Waterloo, Canada,
 * August 2001, pp. 157-160.
 * http://rcvlab.ece.queensu.ca/~greensm/papers/tinn.pdf
 *
 * \tparam T the value_type of the proximity_list. The type of object that is
 * stored in the proximity_list.
 * \tparam Distance the distance function. Must obey the triangle inequality:
 * \f$ \displaystyle \|x + y\| \leq \|x\| + \|y\| \quad \forall \, x, y \in V
 * \f$
 */
template<class T, class Distance>
class proximity_list
{
private:
    typedef typename Distance::result_type result_type;
    class proximity_list_node;
    template<class V>
    class proximity_list_iterator;
    typedef typename std::multiset<proximity_list_node> container;
public:
    //! The type of object, T, stored in the proximity_list.
    typedef T value_type;
    //! Reference to T.
    typedef T & reference;
    //! Const reference to T.
    typedef const T & const_reference;
    //! Pointer to T.
    typedef T * pointer;
    /*!
     * \brief A signed integral type used to represent the distance between two
     * of the proximity_list's iterators.
     */
    typedef typename container::difference_type difference_type;
    /*!
     * \brief An unsigned integral type that can represent any non-negative
     * value of the proximity_list's difference_type.
     */
    typedef typename container::size_type size_type;
    //! Iterator used to iterate through the proximity_list.
    typedef proximity_list_iterator<proximity_list_node> iterator;
    //! Const iterator used to iterate through the proximity_list.
    typedef proximity_list_iterator<const proximity_list_node> const_iterator;
    //! Iterator used to iterate backwards through the proximity_list.
    typedef boost::reverse_iterator<iterator> reverse_iterator;
    //! Const iterator used to iterate backwards through the proximity_list.
    typedef boost::reverse_iterator<const_iterator> const_reverse_iterator;

    //! Creates an empty proximity_list.
    proximity_list();
    //! Creates a proximity_list with a copy of a range.
    template<class RandomAccessIterator>
    proximity_list(RandomAccessIterator first, RandomAccessIterator last);
    //! Returns an iterator pointing to the beginning of the proximity_list.
    iterator begin() const;
    //! Returns an iterator pointing to the end of the proximity_list.
    iterator end() const;
    /*!
     * \brief Returns a reverse_iterator pointing to the beginning of the
     * reversed proximity_list.
     */
    reverse_iterator rbegin() const;
    /*!
     * \brief Returns a reverse_iterator pointing to the end of the
     * reversed proximity_list.
     */
    reverse_iterator rend() const;
    //! Returns the size of the proximity_list.
    size_type size() const;
    //! Inserts value into the proximity_list.
    iterator insert(const T &value);
    //! Inserts a range into the proximity_list.
    template<class RandomAccessIterator>
    void insert(RandomAccessIterator first, RandomAccessIterator last);
    //! Finds an element nearest to value.
    iterator find_nearest(const T &value) const;
private:

    class proximity_list_node:
        boost::less_than_comparable<proximity_list_node>
    {
        friend class proximity_list;
    public:
        proximity_list_node(const T &value, const result_type &distance,
            const size_type &index): value_(value), distance_(distance),
            index_(index)
        {
            //
        }
        bool operator<(const proximity_list_node &other) const
        {
            return distance_ < other.distance_;
        }
    private:
        T value_;
        result_type distance_;
        size_type index_;
    };

    template<class V>
    class proximity_list_iterator: public boost::iterator_adaptor<
        proximity_list_iterator<V>, typename container::const_iterator,
        const_reference, boost::bidirectional_traversal_tag>
    {
        struct enabler {};
    public:
        proximity_list_iterator(): proximity_list_iterator::iterator_adaptor_(
            typename container::const_iterator())
        {
            //
        }
        explicit proximity_list_iterator(typename container::const_iterator p):
            proximity_list_iterator::iterator_adaptor_(p)
        {
            //
        }
        template<class U>
        proximity_list_iterator(const proximity_list_iterator<U> &other,
            typename boost::enable_if<boost::is_convertible<U *, V *>,
            enabler>::type = enabler()):
            proximity_list_iterator::iterator_adaptor_(other.base())
        {
            //
        }
    private:
        friend class boost::iterator_core_access;
        bool equal(const proximity_list_iterator &other) const
        {
            return this->base() == other.base();
        }
        void increment()
        {
            ++this->base_reference();
        }
        void decrement()
        {
            --this->base_reference();
        }
        const_reference dereference() const
        {
            return this->base()->value_;
        }
    };

    bool closer_distance(result_type left, result_type right,
        result_type reference) const;
    container tinn_;
    size_type next_index_;
    Distance distance_function_;
};

template<class T, class Distance>
proximity_list<T, Distance>::proximity_list(): next_index_(0)
{
}
template<class T, class Distance> template<class RandomAccessIterator>
proximity_list<T, Distance>::proximity_list(RandomAccessIterator first,
    RandomAccessIterator last): next_index_(0)
{
    insert(first, last);
}
template<class T, class Distance>
inline typename proximity_list<T, Distance>::iterator proximity_list<T,
    Distance>::begin() const
{
    return iterator(tinn_.begin());
}
template<class T, class Distance>
inline typename proximity_list<T, Distance>::iterator proximity_list<T,
    Distance>::end() const
{
    return iterator(tinn_.end());
}
template<class T, class Distance>
inline typename proximity_list<T, Distance>::reverse_iterator proximity_list<T,
    Distance>::rbegin() const
{
    return reverse_iterator(end());
}
template<class T, class Distance>
inline typename proximity_list<T, Distance>::reverse_iterator proximity_list<T,
    Distance>::rend() const
{
    return reverse_iterator(begin());
}
template<class T, class Distance>
inline typename proximity_list<T, Distance>::size_type proximity_list<T,
    Distance>::size() const
{
    return tinn_.size();
}
template<class T, class Distance>
typename proximity_list<T, Distance>::iterator proximity_list<T,
    Distance>::insert(const T &value)
{
    if(!tinn_.empty())
    {
        proximity_list_node node = proximity_list_node(value,
            distance_function_(*iterator(tinn_.begin()), value), next_index_);
        iterator result = iterator(tinn_.insert(node));
        ++next_index_;

        return result;
    }
    tinn_.insert(proximity_list_node(value, result_type(), next_index_));
    ++next_index_;

    return iterator(tinn_.begin());
}
template<class T, class Distance> template<class RandomAccessIterator>
void proximity_list<T, Distance>::insert(RandomAccessIterator first,
    RandomAccessIterator last)
{
    if(tinn_.empty())
    {
        boost::uniform_int<> size(0, last - first - 1);

        boost::variate_generator<boost::mt19937 &,
            boost::uniform_int<> > random(rng, size);
    
        // FIXME: Make this work for non-random iterators.
        RandomAccessIterator reference = first + random();

        for(; first != last; ++first, ++next_index_)
        {
            tinn_.insert(proximity_list_node(*first,
                distance_function_(*reference, *first), next_index_));
        }
    }
    else
    {
        for(; first != last; ++first, ++next_index_)
        {
            tinn_.insert(proximity_list_node(*first,
                distance_function_(*iterator(tinn_.begin()), *first),
                next_index_));
        }
    }
}
template<class T, class Distance>
typename proximity_list<T, Distance>::iterator proximity_list<T,
    Distance>::find_nearest(
    const T&value) const
{
    typedef typename container::const_iterator base_const_iterator;
    typedef typename container::const_reverse_iterator
        base_const_reverse_iterator;
    
    proximity_list_node query(value, distance_function_(*iterator(
        tinn_.begin()), value), result_type());
    result_type distance_from_reference = distance_function_(
        *iterator(tinn_.begin()), value);

    typename container::const_iterator high = tinn_.lower_bound(query);
    typename container::const_iterator low = high;
    --low;
    typename container::const_iterator pivot;
    if(high == tinn_.end())
    {
        pivot = low;
    }
    else if(high != tinn_.begin() &&
        std::abs(low->distance_ - distance_from_reference) <=
        std::abs(high->distance_ - distance_from_reference))
    {
        pivot = low;
    }
    else
    {
        pivot = high;
    }
    
    typename container::const_iterator closest = pivot;

    result_type min = distance_function_(value, *iterator(pivot));

    result_type current;
    for(base_const_reverse_iterator iter = base_const_reverse_iterator(pivot);
        iter != tinn_.rend(); ++iter)
    {
        if(!closer_distance(distance_from_reference, iter->distance_, min))
        {
            break;
        }
        current = distance_function_(value, iter->value_);
        if(current == min && iter->index_ < closest->index_)
        {
            closest = --iter.base();
        }
        else if(current < min)
        {
            closest = --iter.base();
            min = current;
        }
    }
    for(base_const_iterator iter = pivot; iter != tinn_.end(); ++iter)
    {
        if(!closer_distance(distance_from_reference, iter->distance_, min))
        {
            break;
        }
        current = distance_function_(value, iter->value_);
        if(current == min && iter->index_ < closest->index_)
        {
            closest = iter;
        }
        else if(current < min)
        {
            closest = iter;
            min = current;
        }
    }

    return iterator(closest);
}
template<class T, class Distance>
bool proximity_list<T, Distance>::closer_distance(result_type left,
    result_type right, result_type reference) const
{
    result_type temp = left + right;

    if(temp <= reference)
    {
        return true;
    }
    temp -= reference;

    return temp * temp <= 4 * left * right;
}
