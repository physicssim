#ifndef LESS_LENGTH_SQUARED
#define LESS_LENGTH_SQUARED

#include <boost/numeric/ublas/vector.hpp>

/*!
 * \class LessLengthSquared less_length_squared.hpp "util/less_length_squared.hpp"
 * \brief This functor compares the squared lengths of two vectors.
 */
struct LessLengthSquared
{
    /*!
     * \brief Returns true if lhs is less than rhs.
     *
     * \tparam V1 vector 1 type.
     * \tparam V2 vector 2 type.
     */
    template<class V1, class V2>
    bool operator()(const V1 &lhs, const V2 &rhs)
    {
        return inner_prod(lhs, lhs) < inner_prod(rhs, rhs);
    }
};

#endif // LESS_LENGTH_SQUARED
