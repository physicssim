#ifndef EQUAL_VECTOR_HPP
#define EQUAL_VECTOR_HPP

#include <algorithm>
#include <functional>

template<class T>
struct EqualVector: public std::binary_function<T, T, bool>
{
    bool operator()(const T &lhs, const T &rhs) const
    {
        return std::equal(lhs.begin(), lhs.end(), rhs.begin());
    }
};

#endif // EQUAL_VECTOR_HPP
