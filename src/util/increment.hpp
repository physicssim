/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef INCREMENT_HPP
#define INCREMENT_HPP

/*!
 * \brief This functor increments a value, returning the previous value.
 *
 * \code
 * int main()
 * {
 *     // Fill an array with values 0 .. 9.
 *     int array[10];
 *     generate(array, array + 10, Increment<int>());
 *     copy(array, array + 10, ostream_iterator<int>(cout, " "));
 *     cout << endl; 
 * }
 * \endcode
 */
template<class T>
class Increment
{
public:
    /*!
     * \param value the value from which to start.
     */
    Increment(T value = T()): value_(value)
    {
        //
    }
    //! Increment the value and return the old one.
    T operator()()
    {
        return value_++;
    }
private:
    T value_;
};

#endif // INCREMEMT_HPP
