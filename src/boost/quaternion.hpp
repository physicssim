/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2009 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef QUATERNION_HPP
#define QUATERNION_HPP

#include <boost/math/quaternion.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/serialization/nvp.hpp>
#include <cmath>

namespace boost
{
    namespace math
    {
        template<class T>
        inline boost::numeric::ublas::bounded_vector<T, 3> axis(
            const quaternion<T> &q)
        {
            boost::numeric::ublas::bounded_vector<T, 3> result;
            result(0) = q.R_component_2();
            result(1) = q.R_component_3();
            result(2) = q.R_component_4();

            return normalise(result);
        }
        template<class T>
        inline T angle(const quaternion<T> &q)
        {
            return std::acos(q.real()) * T(2);
        }
        template<class T, class E>
        inline quaternion<T> prod(const quaternion<T> &lhs,
            const boost::numeric::ublas::vector_expression<E> &rhs)
        {
            return lhs * boost::math::quaternion<T>(T(), rhs()(0), rhs()(1),
                rhs()(2));
        }
        template<class T, class E>
        inline quaternion<T> prod(
            const boost::numeric::ublas::vector_expression<E> &lhs,
            const quaternion<T> &rhs)
        {
            return boost::math::quaternion<T>(T(), lhs()(0), lhs()(1),
                lhs()(2)) * rhs;
        }
        template<class T>
        inline quaternion<T> normalise(const quaternion<T> &q)
        {
            T length2 = norm(q);
            if(length2 <= T())
            {
                return quaternion<T>();
            }
            T denom = T(1) / std::sqrt(length2);

            return q * denom;
        }
        template<class T>
        inline boost::numeric::ublas::bounded_matrix<T, 3, 3> toRotation(
            const quaternion<T> &q)
        {
            T n = norm(q);
            T s = n > T() ? T(2) / n : T();

            T xs = q.R_component_2() * s;
            T ys = q.R_component_3() * s;
            T zs = q.R_component_4() * s;
            T wxs = q.R_component_1() * xs;
            T wys = q.R_component_1() * ys;
            T wzs = q.R_component_1() * zs;
            T xxs = q.R_component_2() * xs;
            T xys = q.R_component_2() * ys;
            T xzs = q.R_component_2() * zs;
            T yys = q.R_component_3() * ys;
            T yzs = q.R_component_3() * zs;
            T zzs = q.R_component_4() * zs;

            boost::numeric::ublas::bounded_matrix<T, 3, 3> result;
            result(0, 0) = T(1) - (yys + zzs);
            result(0, 1) = xys - wzs;
            result(0, 2) = xzs + wys;
            result(1, 0) = xys + wzs;
            result(1, 1) = T(1) - (xxs + zzs);
            result(1, 2) = yzs - wxs;
            result(2, 0) = xzs - wys;
            result(2, 1) = yzs + wxs;
            result(2, 2) = T(1) - (xxs + yys);

            return result;
        }
    }
}

#if 0
namespace boost { namespace serialization {

// Serialisation.
template<class Archive, class T>
void save(Archive &ar, const boost::math::quaternion<T> &q,
    const unsigned int /* file_version */)
{
    T a = q.R_component_1();
    T b = q.R_component_2();
    T c = q.R_component_3();
    T d = q.R_component_4();
    ar << boost::serialization::make_nvp("a", a);
    ar << boost::serialization::make_nvp("b", b);
    ar << boost::serialization::make_nvp("c", c);
    ar << boost::serialization::make_nvp("d", d);
}
template<class Archive, class T>
void load(Archive &ar, boost::math::quaternion<T> &q,
    const unsigned int /* file_version */)
{
    T a;
    ar >> boost::serialization::make_nvp("a", a);
    T b;
    ar >> boost::serialization::make_nvp("b", b);
    T c;
    ar >> boost::serialization::make_nvp("c", c);
    T d;
    ar >> boost::serialization::make_nvp("d", d);

    q = boost::math::quaternion<T>(a, b, c, d);
}

template<class Archive, class T>
inline void serialize(Archive & ar, boost::math::quaternion<T> & q,
    const unsigned int file_version)
{
    split_free(ar, q, file_version); 
}

}}
#endif

#endif // QUATERNION_HPP
