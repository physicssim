/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2007 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef INVERT_MATRIX_HPP
#define INVERT_MATRIX_HPP

#if defined(BOOST_UBLAS_TYPE_CHECK)
    #undef BOOST_UBLAS_TYPE_CHECK
#endif
#define BOOST_UBLAS_TYPE_CHECK 0 // Switch automatic singular check off.
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>

namespace boost { namespace numeric { namespace ublas {

template<class M1, class M2>
bool invert(const M1 & m, M2 & inverse)
{
    typedef typename M1::size_type size_type;
    typedef typename M1::value_type value_type;

    size_type size1 = m.size1();
    size_type size2 = m.size2();

    matrix<value_type> temp(m);
    permutation_matrix<> pm(size1);

    if(lu_factorize(temp, pm))
    {
        return false;
    }
    inverse.assign(identity_matrix<value_type>(size1, size2));
    lu_substitute(temp, pm, inverse);

    return true;
}

}}}

#endif // INVERT_MATRIX_HPP
