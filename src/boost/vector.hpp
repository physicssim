/*
 * PhysicsSim - A simple demonstration of collision detection and physics.
 * Copyright (C) 2006-2008 Vitaly Budovski (vbudovski 'at' gmail 'dot' com).
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef VECTOR_HPP

namespace boost { namespace numeric { namespace ublas {

template<class E>
inline bounded_vector<typename E::value_type, 3> normalise(
    const vector_expression<E> &e)
{
    typedef typename E::value_type T;

    T length2 = inner_prod(e(), e());
    if(length2 <= T())
    {
        return T() * e();
    }
    T denom = T(1) / std::sqrt(length2);
    
    return denom * e();
}

template<class E1, class E2>
inline bounded_vector<typename E1::value_type, 3> cross(
    const vector_expression<E1> & e1, const vector_expression<E2> & e2)
{
    bounded_vector<typename E1::value_type, 3> temp;
    temp(0) = e1()(1) * e2()(2) - e1()(2) * e2()(1);
    temp(1) = e1()(2) * e2()(0) - e1()(0) * e2()(2);
    temp(2) = e1()(0) * e2()(1) - e1()(1) * e2()(0);

    return temp;
}

}}}

#define VECTOR_HPP
#endif // VECTOR_HPP
