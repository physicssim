# - Try to find GLEW
# Once done this will define
#  
#  GLEW_FOUND        - system has GLEW
#  GLEW_INCLUDE_DIR  - the GLEW include directory
#  GLEW_LIBRARY    - Link these to use GLEW
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.

IF(WIN32)
  FIND_PATH(GLEW_INCLUDE_DIR GL/glew.h
    /usr/include
    /usr/local/include
  )

  FIND_LIBRARY(GLEW_LIBRARY
    NAMES glew32
    PATHS /usr/lib
          /usr/local/lib
  )
ELSE(WIN32)
  FIND_PATH(GLEW_INCLUDE_DIR GL/glew.h
    /usr/include
    /usr/local/include
  )

  FIND_LIBRARY(GLEW_LIBRARY
    NAMES GLEW
    PATHS /usr/lib
          /usr/local/lib
  )
ENDIF(WIN32)

SET(GLEW_FOUND "NO")
IF(GLEW_LIBRARY)
    SET(GLEW_FOUND "YES")
ENDIF(GLEW_LIBRARY)

# This deprecated setting is for backward compatibility with CMake1.4
SET(GLEW_INCLUDE_PATH ${GLEW_INCLUDE_DIR})

MARK_AS_ADVANCED(
  GLEW_INCLUDE_DIR
  GLEW_LIBRARY
)
